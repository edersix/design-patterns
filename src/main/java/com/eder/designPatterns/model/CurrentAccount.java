package com.eder.designPatterns.model;

/**
 * @author cesar.cruz
 *
 */
public class CurrentAccount implements Account {

	@Override
	public void accountType() {
		System.out.println("CURRENT ACCOUNT");
	}

}
