package com.eder.designPatterns.model;

/**
 * @author cesar.cruz
 *
 */
public class YesBank implements Bank{

	@Override
	public void bankName() {
		System.out.println("Yes Bank Pvt. Ltd.");
	}

}
