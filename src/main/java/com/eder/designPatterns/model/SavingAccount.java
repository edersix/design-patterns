package com.eder.designPatterns.model;

/**
 * @author cesar.cruz
 *
 */
public class SavingAccount implements Account{

	@Override
	public void accountType() {
		System.out.println("SAVING ACCOUNT");
	}

}
