package com.eder.designPatterns.model;

/**
 * @author cesar.cruz
 *
 */
public class ICICIBank implements Bank {

	@Override
	public void bankName() {
		System.out.println("ICICI Bank Ltd.");
	}

}
