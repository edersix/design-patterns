package com.eder.designPatterns.flyweight.pattern;

import com.eder.designPatterns.model.Account;

/**
 * @author cesar.cruz
 *
 */
public class FlyweightPatternClient {

	public static void main(String[] args) {
		for(int i=0; i < 10; ++i) {
			Account account = (Account)AccountFlyweightFactory.getAccount("SAVING");
			account.accountType();
		}
	}
}
