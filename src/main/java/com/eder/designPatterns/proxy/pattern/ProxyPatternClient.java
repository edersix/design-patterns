package com.eder.designPatterns.proxy.pattern;

import com.eder.designPatterns.model.Account;

/**
 * @author cesar.cruz
 *
 */
public class ProxyPatternClient {

	public static void main(String[] args) {
		Account account = new ProxySavingAccount();
		account.accountType();
	}

}
