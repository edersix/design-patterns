package com.eder.designPatterns.proxy.pattern;

import com.eder.designPatterns.model.Account;
import com.eder.designPatterns.model.SavingAccount;

/**
 * @author cesar.cruz
 *
 */
public class ProxySavingAccount implements Account{
	
	private Account savingAccount;
	
	@Override
	public void accountType() {
		if(savingAccount == null){
			savingAccount = new SavingAccount();
		}
		savingAccount.accountType();
	}

}
