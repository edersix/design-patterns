/**
 * 
 */
package com.eder.designPatterns.observer.pattern;

/**
 * @author cesar.cruz
 *
 */
public interface Observer {
	
	public void update();
		
	public void setSubject(Subject sub);
}
