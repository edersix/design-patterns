package com.eder.designPatterns.abstractfactory.pattern;

import com.eder.designPatterns.model.Account;
import com.eder.designPatterns.model.Bank;

/**
 * @author cesar.cruz
 *
 */
public abstract class AbstractFactory {
	
	abstract Bank getBank(String bankName);
	
	abstract Account getAccount(String accountType);
}
