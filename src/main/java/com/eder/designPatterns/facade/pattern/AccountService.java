package com.eder.designPatterns.facade.pattern;

import com.eder.designPatterns.model.Account;
import com.eder.designPatterns.model.SavingAccount;

/**
 * @author cesar.cruz
 *
 */
public class AccountService {

	public static Account getAccount(String accountId) {
		return new SavingAccount();
	}
}
