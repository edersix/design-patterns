package com.eder.designPatterns.facade.pattern;

import com.eder.designPatterns.model.Account;

/**
 * @author cesar.cruz
 *
 */
public class TransferService {

	public static void transfer(int amount, Account fromAccount, Account toAccount) {
		System.out.println("Transfering Money");
	}
}
