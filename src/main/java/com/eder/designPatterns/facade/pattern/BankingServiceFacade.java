package com.eder.designPatterns.facade.pattern;

public interface BankingServiceFacade {
	void moneyTransfer();
}
