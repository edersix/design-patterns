package com.eder.designPatterns.facade.pattern;

/**
 * @author cesar.cruz
 *
 */
public class FacadePatternClient {
	public static void main(String[] args) {
		BankingServiceFacade serviceFacade = new BankingServiceFacadeImpl();
		serviceFacade.moneyTransfer();
	}

}
