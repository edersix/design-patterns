package com.eder.designPatterns.decorator.pattern;

/**
 * @author cesar.cruz
 *
 */
public abstract class AccountDecorator implements Account{
	
	abstract String applyOtherBenefits();

}
