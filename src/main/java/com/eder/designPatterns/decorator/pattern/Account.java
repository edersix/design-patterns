package com.eder.designPatterns.decorator.pattern;

public interface Account {
	String getTotalBenefits();
}
