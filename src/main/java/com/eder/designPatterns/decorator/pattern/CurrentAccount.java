package com.eder.designPatterns.decorator.pattern;

/**
 * @author cesar.cruz
 *
 */
public class CurrentAccount implements Account{
	
	@Override
	public String getTotalBenefits() {
		return "There is no withdrwal limit for current account";
	}

}
