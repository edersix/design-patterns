package com.eder.designPatterns.prototype.pattern;

/**
 * @author cesar.cruz
 *
 */
public class CurrentAccount extends Account {
	@Override
	public void accountType() {
		System.out.println("CURRENT ACCOUNT");
	}
}
