package com.eder.designPatterns.prototype.pattern;

/**
 * @author cesar.cruz
 *
 */
public class SavingAccount extends Account{
	@Override
	public void accountType() {
		System.out.println("SAVING ACCOUNT");
	}
}
