package com.eder.designPatterns.bridge.pattern;

/**
 * @author cesar.cruz
 *	Implementor for bridge pattern
 */
public interface Account {
	Account openAccount();
	void accountType();
}
