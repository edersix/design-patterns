package com.eder.designPatterns.adapter.pattern;

import com.eder.designPatterns.model.Account;

public interface PaymentGateway {
	void doPayment(Account account1, Account account2);
}
