package com.eder.designPatterns.adapter.pattern;

public interface AdvancedPayGateway {
	
	void makePayment(String mobile1, String mobile2);
	
}
