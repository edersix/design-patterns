package com.eder.designPatterns.adapter.pattern;

import com.eder.designPatterns.model.Account;

/**
 * @author cesar.cruz
 *
 */
public class PaymentGatewayImpl implements PaymentGateway{
	@Override
	public void doPayment(Account account1, Account account2){
		System.out.println("Do payment using Payment Gateway");
	}
	
}
