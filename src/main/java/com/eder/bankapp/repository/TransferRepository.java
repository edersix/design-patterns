package com.eder.bankapp.repository;

import com.eder.bankapp.model.Account;
import com.eder.bankapp.model.Amount;

public interface TransferRepository {
	
	void transfer(Account accountA, Account accountB, Amount amount);
}
