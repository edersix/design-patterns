package com.eder.bankapp.repository.jdbc;

import org.springframework.stereotype.Repository;

import com.eder.bankapp.model.Account;
import com.eder.bankapp.model.Amount;
import com.eder.bankapp.repository.AccountRepository;
@Repository
public class JdbcAccountRepository implements AccountRepository {

	@Override
	public Account findByAccountId(Long accountId) {
		return new Account(accountId, "Arnav Rajput", new Amount(3000.0));
	}

}
