package com.eder.bankapp.repository.jdbc;

import org.springframework.stereotype.Repository;

import com.eder.bankapp.model.Account;
import com.eder.bankapp.model.Amount;
import com.eder.bankapp.repository.TransferRepository;
@Repository
public class JdbcTransferRepository implements TransferRepository {

	@Override
	public void transfer(Account accountA, Account accountB, Amount amount) {
		System.out.println("Transfering amount from account A to B via JDBC implementation");
	}

}
