package com.eder.bankapp.repository;

import com.eder.bankapp.model.Account;

public interface AccountRepository {
	
	Account findByAccountId(Long accountId);
}
