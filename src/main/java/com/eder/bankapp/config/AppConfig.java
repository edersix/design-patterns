package com.eder.bankapp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.eder.bankapp.repository.AccountRepository;
import com.eder.bankapp.repository.TransferRepository;
import com.eder.bankapp.repository.jdbc.JdbcAccountRepository;
import com.eder.bankapp.repository.jdbc.JdbcTransferRepository;
import com.eder.bankapp.service.TransferService;
import com.eder.bankapp.service.TransferServiceImpl;

@Configuration
@ComponentScan(basePackageClasses={TransferService.class,AccountRepository.class})
public class AppConfig {
	
	@Bean
	public TransferService transferService(){
		return new TransferServiceImpl(accountRepository(), transferRepository());
	}
	@Bean
	public AccountRepository accountRepository() {
		return new JdbcAccountRepository();
	}
	@Bean
	public TransferRepository transferRepository() {
		return new JdbcTransferRepository();
	}
}
